<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Manager.
 *
 * @package    mod_courseguide;
 * @copyright  2017 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @author     Jerome Mouneyrac <jerome@bepaw.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_courseguide;
defined('MOODLE_INTERNAL') || die();

use context_course;
use stdClass;
use block_moderator_guide\manager_base;
use block_moderator_guide\guide_base;

/**
 * Manager class.
 *
 * The manager for the block.
 *
 * @package    mod_courseguide;
 * @copyright  2017 onwards Coventry University {@link http://www.coventry.ac.uk/}
 * @author     Jerome Mouneyrac <jerome@bepaw.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class manager extends manager_base {

    /**
     * Return the component.
     *
     * @return string
     */
    public function get_component() {
        return 'mod_courseguide';
    }

    /**
     * Return the template table.
     *
     * @return string
     */
    public function get_templates_table() {
        return 'mod_courseguide_templates';
    }

    /**
     * Return the guides table.
     *
     * @return string
     */
    public function get_guides_table() {
        return 'mod_courseguide_guides';
    }

    /**
     * Return the guide contents table.
     *
     * @return string
     */
    public function get_guide_contents_table() {
        return 'mod_courseguide_contents';
    }

    /**
     * Return the template class to use.
     *
     * @return string
     */
    public function get_template_class() {
        return 'mod_courseguide\\template';
    }

    /**
     * Return the guide class to use.
     *
     * @return string
     */
    public function get_guide_class() {
        return 'mod_courseguide\\guide';
    }

    /**
     * Get guide SQL order.
     *
     * @return string
     */
    public function get_guide_sql_order() {
        return 'id';
    }

    /**
     * Get the context of a guide.
     *
     * @param guide $guide The guide.
     * @return context
     */
    public function get_guide_context(\block_moderator_guide\guide_base $guide) {
        return context_course::instance($guide->get_courseid());
    }

    /**
     * Get the profile field to restrict templates on.
     *
     * @return null|string
     */
    protected function get_restriction_profile_field() {
        $restrictionfield = get_config('block_moderator_guide', 'restriction');
        return !empty($restrictionfield) ? $restrictionfield : null;
    }

    /**
     * Process a template's form data.
     *
     * @param template_base $template The template.
     * @param stdClass $formdata Form data.
     */
    protected function process_template_form_data(\block_moderator_guide\template_base $template, stdClass $data) {
        parent::process_template_form_data($template, $data);

        // set the default display mode if displaymode is not 'any'
        if ($data->displaymode !== 'any') {
            $data->defaultdisplaymode = $data->displaymode;
        }

        $template->import_record((object) ['defaultguidename' => $data->defaultguidename,
            'displaymode' => $data->displaymode, 'defaultdisplaymode' => $data->defaultdisplaymode]);
    }

    /**
     * Get a guide by cmid.
     *
     * @param int $cmid The Course Guide instance ID.
     * @return guide_base
     */
    public function get_guide_by_instanceid($courseguideid) {
        global $DB;
        $guideid = $DB->get_field($this->get_guides_table(), 'id', ['courseguideid' => $courseguideid]);
        return $this->get_guide($guideid);
    }

    /**
     * Save a guide.
     *
     * @param guide_base $guide The guide
     * @param stdClass[] $files Information about the files (draftitemid and filearea).
     * @return guide_base A new guide instance.
     */
    public function save_guide(guide_base $guide, array $files = []) {
        global $DB;

        // update the activity name.
        $record = $guide->get_record();
        if (!empty($record->name) && !empty($record->courseguideid)) {
            $courseguide = new stdClass();
            $courseguide->id = $record->courseguideid;
            $courseguide->name = $record->name;
            $DB->update_record('courseguide', $courseguide);
            rebuild_course_cache($guide->get_courseid(), true);
        }

        return parent::save_guide($guide, $files);
    }

    /**
     * Function to serve pluginfiles.
     *
     * This should be called from within the plugin's pluginfile function.
     *
     * @return void
     */
    public function serve_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload,
                                     array $options = array(), array $permissions = array()) {

        parent::serve_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload,
            $options, array('system' => 'mod/courseguide:viewguide', 'context' => 'mod/courseguide:viewguide'));

    }

}

